package mx.raul.spring.security.jwtsecurity.filter;

import lombok.RequiredArgsConstructor;
import mx.raul.spring.security.jwtsecurity.util.JWTUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class JWTFilter extends OncePerRequestFilter {

  private final JWTUtil jwtUtil;
  private final UserDetailsService userDetailsService;

  @Override
  protected void doFilterInternal(final HttpServletRequest httpServletRequest,
                                  final HttpServletResponse httpServletResponse,
                                  final FilterChain filterChain) throws ServletException, IOException {
    final Optional<String> authorizationHeaderValue = Optional.ofNullable(httpServletRequest.getHeader("Authorization"));

    authorizationHeaderValue
        .filter(headerValue -> headerValue.startsWith("Bearer "))
        .ifPresent(headerValue -> {
          final String jwt = headerValue.substring(7);
          final String username = jwtUtil.extractUsername(jwt);

          if (!Objects.isNull(username) && Objects.isNull(SecurityContextHolder.getContext().getAuthentication())) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);

            if (Boolean.TRUE.equals(jwtUtil.validateToken(jwt, userDetails))) {
              UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                  new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
              usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
              SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
          }
        });
    filterChain.doFilter(httpServletRequest, httpServletResponse);
  }
}
