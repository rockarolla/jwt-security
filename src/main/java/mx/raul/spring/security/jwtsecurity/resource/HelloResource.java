package mx.raul.spring.security.jwtsecurity.resource;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mx.raul.spring.security.jwtsecurity.dto.AuthenticationRequest;
import mx.raul.spring.security.jwtsecurity.dto.AuthenticationResponse;
import mx.raul.spring.security.jwtsecurity.util.JWTUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class HelloResource {
  
  private final AuthenticationManager authenticationManager;
  private final UserDetailsService userDetailsService;
  private final JWTUtil jwtUtil;

  @RequestMapping({"/hello"})
  public String hello() {
    return "Hello!";
  }
  
  @PostMapping(value = "/authenticate")
  public ResponseEntity<AuthenticationResponse> authenticate(
      @RequestBody AuthenticationRequest authenticationRequest) throws Exception {
    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
          authenticationRequest.getUsername(), authenticationRequest.getPassword()));
    }
    catch (BadCredentialsException ex) {
      log.error("Bad credentials", ex);
      throw new Exception("Bad credentials", ex);
    }
    
    final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
    final String jwt = jwtUtil.generateToken(userDetails);
    return ResponseEntity.ok(new AuthenticationResponse(jwt));
  }
  
}
